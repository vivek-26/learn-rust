fn main() {
  // Both `if` and `else` block should return the same type.
  let num = if 3 > 2 {
    println!("If block");
    // Not using semicolon after a statement means
    // you want to return that value.
    1
  } else {
    println!("Else block");
    2
    // Semicolon is compulsory.
  };
  println!("Value of num is {}", num);

  // In Rust, no value type is --> ()
  let num = if 3 > 2 {
    // Not adding a else block means it is --> ()
    ()
  };
  println!("{:?}", num);
}
