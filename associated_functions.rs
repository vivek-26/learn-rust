// Associated Function
// When we define a function within `impl` block that doesn't take
// `self` as a parameter, they are called associated functions.
// They are associated with struct.

// Associated functions are often used for constructors that will return
// a new instance of the struct.

#[derive(Debug)]
struct Shape {
  width: u16,
  height: u16,
}

impl Shape {
  fn square(size: u16) -> Shape {
    Shape {
      width: size,
      height: size,
    }
  }
}

fn main() {
  println!("{:?}", Shape::square(50));
}
