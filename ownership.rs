// Ownership rules
// Rule 1 - Each value in Rust has a variable that's called it's owner.

// Rule 2 - There can only be 1 owner at a time. You can't create two variables
// which point to same memory location.

// Rule 3 - When owner goes out of scope, the value will be dropped.
// Rust calls a special function called `drop` which releases memory.

// Understanding `move` in Rust

fn main() {
  // This works, since the value is in Stack.
  let a = 1;
  let b = a;
  println!("This works --> {} {}", a, b);

  // This does not work, the value is in Heap.
  let str_1 = String::from("Hello");
  // let str_2 = str_1; // This statement `moves` str_1 to str_2.
  // str_1 is invalidated.
  println!("{}", str_1);

  // Deep cloning strings
  let str_2 = str_1.clone(); // str_1 and str_2 have different memory location.
  println!("{} {}", str_1, str_2);

  let str_3 = String::from("New String");
  let str_4 = ownership_and_functions(str_3);
  // Printing str_3 will throw error as it is moved.
  println!("{}", str_4);
}

// Ownership & Functions
// Passing a variable to a function will move or copy, just as assignment does.
// Basically, passing a variable to a function, also passes its ownership to
// that function.
// Returning values from a function, also transfers ownership.

fn ownership_and_functions(str_1: String) -> String {
  println!("Received String: {}", str_1);
  // Return transfers the ownership back
  str_1
}
