fn plus_one(x: Option<u32>) -> Option<u32> {
  match x {
    None => None,
    Some(i) => Some(i + 1),
  }
}

fn placeholder(x: Option<u32>) -> Option<u32> {
  match x {
    None => None,
    _ => None,
  }
}

fn main() {
  let five = Some(5);
  let six = plus_one(five);
  let none = plus_one(None);

  println!("{:?} {:?} {:?} {:?}", five, six, none, placeholder(Some(1)));

  // If-let in Rust
  let some_u8 = Some(3);
  if let Some(3) = some_u8 {
    println!("Three");
  }
}
