#[derive(Debug)]

enum IPAddrKind {
  V4,
  V6,
}

#[derive(Debug)]
enum IPAddressKind {
  V4(u8, u8, u8, u8),
  V6(String),
}

#[derive(Debug)]
struct IP {
  kind: IPAddrKind,
  address: String,
}

fn main() {
  let four = IPAddrKind::V4;
  let six = IPAddrKind::V6;

  println!("{:?}\n{:?}", four, six);

  route(four);
  route(six);

  let home = IP {
    kind: IPAddrKind::V4,
    address: String::from("127.0.0.1"),
  };

  let loopback = IP {
    kind: IPAddrKind::V6,
    address: String::from("::1"),
  };

  println!("{:?}\n{:?}", home, loopback);

  let home_ip = IPAddressKind::V4(127, 0, 0, 1);
  let loopback_ip = IPAddressKind::V6(String::from("::1"));
  println!("{:?}\n{:?}", home_ip, loopback_ip);
}

fn route(ip: IPAddrKind) {
  println!("{:?}", ip);
}
