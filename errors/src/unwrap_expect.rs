use std::fs::File;

pub fn unwrap() {
  // File::open returns an Result enum. This enum has a method called unwrap()
  // which calls panic in case of errors. It returns Ok, if there's no error.
  let f = File::open("random_file.txt").unwrap();
}

pub fn expect() {
  // Same as unwrap, but allows us to add our own error messages.
  let f = File::open("random_file.txt").expect("Failed to read random file");
}
