use std::fs::File;
use std::io;
use std::io::Read;

// This function propagates error.
pub fn read() -> Result<String, io::Error> {
  let f = File::open("hello.txt");
  let mut f = match f {
    Ok(file) => file,
    Err(e) => return Err(e),
  };

  let mut s = String::new();
  match f.read_to_string(&mut s) {
    Ok(_) => Ok(s),
    Err(e) => Err(e),
  }
}

// Error Propagation using Question Mark Operator
// `?` operator only works when return type is Result enum.
// It automatically return errors. Success case has to be explicitly handled.
pub fn question_mark_operator() -> Result<String, io::Error> {
  let mut f = File::open("hello.txt")?;
  let mut s = String::new();
  f.read_to_string(&mut s)?;
  Ok(s)
}
