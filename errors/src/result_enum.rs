use std::fs::File;
use std::io::ErrorKind;

// Result Enum is used in Rust for recoverable errors.
// Result enum has `Ok` and `Err` values.

pub fn error_enum() {
  let f = File::open("hello.txt");
  let f = match f {
    Ok(file) => file,
    Err(error) => panic!(error),
  };
  println!("{:?}", f);
}

pub fn mataching_different_errors() {
  let f = File::open("rust.txt");
  let f = match f {
    Ok(file) => file,
    Err(ref error) if error.kind() == ErrorKind::NotFound => match File::create("rust.txt") {
      Ok(fh) => fh,
      Err(e) => {
        panic!("Not able to create file, Reason --> {:?}", e);
      }
    },
    Err(error) => {
      panic!("Unable to open file, Reason --> {:?}", error);
    }
  };
}
