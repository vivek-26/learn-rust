extern crate errors;

use errors::error_propagation;
use errors::result_enum;
use errors::unwrap_expect;

fn main() {
  result_enum::error_enum();
  result_enum::mataching_different_errors();

  unwrap_expect::unwrap();
  unwrap_expect::expect();

  let f = error_propagation::read().unwrap();
  println!("Error Propagation --> {:?}", f);

  let f = error_propagation::question_mark_operator().unwrap();
  println!("Error Propagation --> {:?}", f);
}
