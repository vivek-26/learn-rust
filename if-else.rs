use std::io;

fn main() {
  let a: u8 = 10;
  if a % 2 == 0 {
    println!("Even");
  } else {
    println!("Odd");
  }

  // User input with choice y/N
  let mut user_choice = String::new();
  println!("Are your friends coming? ");
  io::stdin()
    .read_line(&mut user_choice)
    .expect("Failed to read input!");

  let user_choice = user_choice.trim();
  if user_choice == "y" {
    println!("Yay, going for a movie!");
  } else {
    println!("Stay at home!");
  }
}
