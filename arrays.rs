fn main() {
  // Uninitialized values cannot be used in Rust.
  let a = [22, 34, 65, 3];
  println!("{:?}", a);

  let a: [u16; 4] = [22, 34, 65, 3];
  println!("{:?}", a);

  let a: [u16; 4] = [0; 4];
  println!("{:?}", a);

  let mut a: [u16; 4] = [0; 4];
  a[0] = 1;
  println!("{:?}", a);

  print(a);

  print_iter(a);
  println!("{}", a.len());
}

fn print(x: [u16; 4]) {
  for i in 0..4 {
    print!("{} ", x[i]);
  }
  println!("");
}

fn print_iter(x: [u16; 4]) {
  for i in x.iter() {
    print!("{} ", i);
  }
  println!("");
}
