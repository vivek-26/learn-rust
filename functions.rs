// Types of functions in Rust.
// No Arg, No Return Type. fn name() {}
// Multiple Arg, No Return Type. fn name(args) {}
// Multiple Arg, One Return Type. fn name(args)->return_type {}
// Multiple Arg, Multiple Return Type. fn name(args)->(return_type, return_type {}
// Multiple Arg, Multiple Return Type. fn name(args) { fn another() {} }

// Functions can be defined anywhere, order is not important.
fn main() {
  add();
  add_with_args(2, 3);
  println!("{}", add_with_return(2, 3));
  println!("{:?}", add_with_multi_return(3, 2));
}

fn add() {
  println!("No Arg, No Return Type");
}

fn add_with_args(a: u8, b: u8) {
  println!("{} + {} = {}", a, b, a + b);
}

fn add_with_return(a: u8, b: u8) -> u8 {
  // No semicolon is needed as we have to return this value.
  // Same as --> return a + b;
  a + b
}

fn add_with_multi_return(a: u8, b: u8) -> (u8, u8) {
  (a + b, a - b)
}
