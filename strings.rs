fn main() {
  // String slices can't be mutated.
  let a = "This is a string slice!";

  let mut b = String::new(); // Creates an empty string.
  b = String::from("Strings in Rust");
  b.push_str(", are a bit tricky!");
  println!("{}", b);
}
