// Slices work with strings and arrays.
fn main() {
  let s = String::from("Hello World");

  let s1 = &s[0..5];
  println!("{}", s1);

  let s1 = &s[2..7];
  println!("{}", s1);

  let s1 = &s[5..];
  println!("{}", s1);

  let s1 = &s[..5];
  println!("{}", s1);

  let s1 = &s[..];
  println!("{}", s1);
}
