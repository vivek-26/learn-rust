#[derive(Debug)]
enum States {
  Alaska,
  Arizona,
}

enum Coin {
  Penny,
  Nickel,
  Dime,
  Quarter(States),
}

fn value_in_cents(coin: Coin) -> u32 {
  match coin {
    Coin::Penny => 1,
    Coin::Nickel => 5,
    Coin::Dime => 10,
    Coin::Quarter(state) => {
      println!("State: {:?}", state);
      25
    }
  }
}

fn main() {
  value_in_cents(Coin::Quarter(States::Arizona));
}
