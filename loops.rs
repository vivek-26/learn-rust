fn main() {
  // Basic Loop
  let mut num: u8 = 1;

  loop {
    if num <= 5 {
      println!("Hello {}", num);
      num += 1;
    } else {
      break;
    }
  }

  // While Loop
  let mut num: u8 = 1;

  while num <= 5 {
    println!("Hello {}", num);
    num += 1;
  }

  // For Loop - It is faster than while loop.
  // Range is exclusive of last value.
  for num in 1..10 {
    // num is only accessible here in this scope.
    print!("{} ", num);
  }
}
