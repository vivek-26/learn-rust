extern crate common_collections;

use common_collections::hashmaps;
use common_collections::strings;
use common_collections::vector;

fn main() {
  vector::vector_code();
  vector::vector_macro();
  vector::reading_vector();
  vector::read_vector_using_for();
  vector::modify_vector_using_for();
  vector::store_multiple_types_vector();

  strings::string_concat();
  strings::string_concat_using_format();
  strings::string_chars();

  hashmaps::hashmaps_basic();
  hashmaps::hashmap_collect();
  hashmaps::hashmap_get_values();
  hashmaps::hashmap_update();
}
