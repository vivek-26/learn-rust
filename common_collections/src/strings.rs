pub fn string_concat() {
  let str_1 = String::from("Rust is an awesome");
  let str_2 = String::from(" language!");
  // str_1 will now be `moved`.
  // Reference of str_2 is passed, since `+` is an function (Trait).
  let str_3 = str_1 + &str_2;

  println!("Concatenated String --> {}", str_3);
}

pub fn string_concat_using_format() {
  let str_1 = String::from("Rust is an awesome");
  let str_2 = String::from("language!");
  // `format!` does not take ownership.
  let str_3 = format!("{} {}", str_1, str_2);
  println!("Concatenated String --> {}", str_3);
}

pub fn string_chars() {
  let s = String::from("Rust is an awesome language!");
  for i in s.chars() {
    print!("{} ", i);
  }
  println!();
}
