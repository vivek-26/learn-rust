pub fn vector_code() {
  let mut v: Vec<u8> = Vec::new();
  v.push(20);
  v.push(30);
  v.push(50);
  println!("Vector --> {:?}", v);
}

pub fn vector_macro() {
  let v: Vec<u8> = vec![20, 30, 50];
  println!("Vector using macro vec! --> {:?}", v);
}

pub fn reading_vector() {
  let v = vec![20, 30, 50];
  let val = v[0];
  // let val = v[100]; // Throws runtime error.
  // vector.get(index) returns Some(&T) or None.
  println!("Reading data from vector");
  let get_val = v.get(0);
  println!("val[0]: {:?}", val);
  println!("v.get(0): {:?}", get_val);
  let get_val = v.get(100);
  println!("v.get(0): {:?}", get_val);
}

pub fn read_vector_using_for() {
  let v = vec![20, 30, 50];
  println!("Reading vector data using for loop");
  for i in &v {
    // using just `v` will cause the value to `move`.
    print!("{:?} ", i);
  }
  println!();
}

pub fn modify_vector_using_for() {
  let mut v = vec![20, 30, 50];
  println!("Modify Vector using for loop");
  println!("Initial Vector: {:?}", v);

  for i in &mut v {
    *i *= 5;
  }

  println!("Modified Vector: {:?}", v);
}

#[derive(Debug)]
enum MultipleTypes {
  Integer(u8),
  Float(f64),
  Bool(bool),
}

pub fn store_multiple_types_vector() {
  let v = vec![
    MultipleTypes::Integer(100),
    MultipleTypes::Float(100.00),
    MultipleTypes::Bool(true),
  ];
  println!("Storing multiple types in vector using enums --> {:?}", v);
}
