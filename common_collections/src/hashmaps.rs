use std::collections::HashMap;

pub fn hashmaps_basic() {
  let mut scores = HashMap::new();
  scores.insert("A", 10);
  scores.insert("B", 20);
  println!("HashMaps --> {:?}", scores);
}

pub fn hashmap_collect() {
  let team = vec!["A", "B"];
  let scores = vec![10, 20];
  let map: HashMap<_, _> = team.iter().zip(scores.iter()).collect();
  println!("Constructing HashMap using collect() method --> {:?}", map);
}

// Accessing values in HashMap
pub fn hashmap_get_values() {
  let mut scores = HashMap::new();
  scores.insert("A", 10);
  scores.insert("B", 20);

  println!("Team A Score: {:?}", scores.get("A"));

  // Using for loop to iterate over HashMap
  for (k, v) in &scores {
    println!("Key: {}, Value: {}", k, v);
  }
}

pub fn hashmap_update() {
  let mut scores = HashMap::new();
  scores.insert("A", 10);
  scores.insert("A", 20); // The value of `A` is overwritten.
  scores.insert("B", 30);

  scores.entry("B").or_insert(50);

  println!("HashMap Update --> {:?}", scores);
}
