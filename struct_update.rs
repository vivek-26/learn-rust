#[derive(Debug)]

struct User {
  name: String,
  age: u8,
  hobby: String,
}

// Struct Update Syntax is used to copy the value from one struct to another.
fn main() {
  let user_1 = User {
    name: String::from("John"),
    age: 24,
    hobby: String::from("Programming"),
  };

  let user_2 = User {
    name: String::from("Jane"),
    age: user_1.age,
    hobby: user_1.hobby.clone(), // user_1.hobby will partially `move` the value
  };

  println!("Without Struct Update Syntax: {:?}\n{:?}", user_1, user_2);

  let user_3 = User {
    name: String::from("James"),
    ..user_1
  };

  println!("With Struct Update Syntax: {:?}", user_3);
}
