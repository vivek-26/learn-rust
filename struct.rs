#[derive(Debug)]
struct User {
  username: String,
  age: u8,
}

fn main() {
  let user = User {
    username: String::from("vivek.kumar26@live.com"),
    age: 24,
  };

  println!("{:?}", user);

  // To get specific value from struct, use dot notation
  // To change value of field using dot notation, the instance
  // needs to be mutable. Rust does not allow field level mutation.
  // Entire instance will be mutable.
  println!("{}", user.username);

  // Returning instance of struct from a function.
  println!("{:?}", build_instance(String::from("Username"), 20));
  println!("{:?}", field_init_shorthand(String::from("Username"), 20));
}

fn build_instance(uname: String, age: u8) -> User {
  User {
    username: uname,
    age: age,
  }
}

fn field_init_shorthand(username: String, age: u8) -> User {
  User { username, age }
}
