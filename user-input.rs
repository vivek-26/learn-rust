use std::io;

fn main() {
  let mut a = String::new();
  println!("Enter a string: ");
  io::stdin().read_line(&mut a).expect("Failed");
  println!("User entered: {} This is in new line", a);

  // io module reads character `\n` as well.
  let mut b = String::new();
  println!("Enter String 'Hello': ");
  io::stdin().read_line(&mut b).expect("Failed");
  let b: String = b.trim().parse().expect("Failed to parse");
  println!("User entered: {} World", b);
  let b: u32 = b.trim().parse().expect("Failed to parse");
  println!("User entered: {} World", b);
}
