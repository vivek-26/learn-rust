// Methods are similar to functions. They take parameters and return value.
// Methods cannot be used without an instance of struct.
// Their first parameter is always `self`. It refers to the instance itself.

#[derive(Debug)]

struct Rectangle {
  width: u16,
  height: u16,
}

impl Rectangle {
  // Always get reference to self, otherwise, the value will be `moved`.
  fn area(&self) -> u16 {
    self.width * self.height
  }

  fn can_hold(&self, other: &Rectangle) -> bool {
    self.width > other.width && self.height > other.height
  }
}

fn main() {
  let rect = Rectangle {
    width: 50,
    height: 50,
  };

  let small_rect = Rectangle {
    width: 40,
    height: 40,
  };

  let large_rect = Rectangle {
    width: 70,
    height: 70,
  };

  println!("Area: {}", rect.area());
  println!("{}", rect.can_hold(&large_rect));
  println!("{}", rect.can_hold(&small_rect));
}
