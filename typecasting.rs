fn main() {
  // Method 1
  let a: i32 = 10;
  let b: i64 = a as i64;

  // Method 2
  let c: i32 = 10;
  let d: i64 = c.into();

  // Typecasting with Arithemtic
  let e: i32 = 10;
  let f: i64 = e as i64 + 10;
  println!("{},{},{},{},{},{}", a, b, c, d, e, f);

  // Typecasting along with shadowing.
  let r: u32 = 100;
  println!("Typecasting before Shadowing, r --> {}", r);
  let r: u64 = r as u64 + 50;
  println!("Typecasting after Shadowing, r --> {}", r);
}
