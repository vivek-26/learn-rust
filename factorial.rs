fn main() {
  println!("Factorial of 6 is {}", fact(6));
}

fn fact(num: u8) -> u16 {
  let mut result: u16 = 1;

  for n in 1..num + 1 {
    result *= n as u16;
  }
  // Return result
  result
}
