use std::io;

fn main() {
  // Print all even numbers from 1 to 100.
  for num in 1..101 {
    if num % 2 == 0 {
      print!("{} ", num);
    }
  }
  println!("");

  // Quiz.
  let mut ans = String::new();
  println!("Which is the longest river in the world ?");
  for num in 1..4 {
    ans.clear();
    io::stdin()
      .read_line(&mut ans)
      .expect("Failed to read input");
    ans = ans.trim().to_string();
    if ans == "Nile" {
      println!("Correct!");
      break;
    }
  }

  // Count number of digits in a number.
  let mut num: u8 = 70;
  let mut count: u8 = 0;

  while num != 0 {
    num /= 10;
    count += 1;
  }

  println!("The length of number is --> {}", count);
}
