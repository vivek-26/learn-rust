// References Rules - These rules are there to avoid data races.
// Rule 1 - You can create any number of immutable references.
// Rule 2 - You can create only one mutable reference in a scope.
// Rule 3 - You cannot create mutable reference if your program uses
// immutable references. Basically, in a scope, you cannot have both
// mutable and immutable reference.

// Dangling References
// Pointer that references a location in memory that doesn't exist.

// Note: This code won't compile.
fn main() {
  let a = dangle();
}

fn dangle() -> &String {
  let s = String::from("Rust is secure");
  &s
} // variable s goes out of scope and is dropped by Rust.
