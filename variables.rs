const MAX: u8 = 10;

fn main() {
  let a = "Hello";
  println!("{} World", a);

  let b: u8 = 257;
  println!("{}", b);

  let c: f32 = 257.3332323;
  println!("{}", c);

  let d: bool = true;
  println!("{}", d);

  let e: char = 'A';
  println!("{}", e);

  let mut f: u8 = 5;
  println!("Value of variable f before mutation ==> {}", f);
  f = 10;
  println!("Value of variable f after mutation ==> {}", f);

  println!("{}", MAX);
}
