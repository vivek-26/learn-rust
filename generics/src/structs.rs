#[derive(Debug)]
struct Point<T> {
  x: T,
  y: T,
}

impl<T> Point<T> {
  fn get_coordinates(&self) -> (&T, &T) {
    (&self.x, &self.y)
  }
}

#[derive(Debug)]
struct CustomPoint<T, E> {
  x: T,
  y: E,
}

impl<T, E> CustomPoint<T, E> {
  fn get_coordinates(&self) -> (&T, &E) {
    (&self.x, &self.y)
  }
}

pub fn point() {
  let p = Point { x: 5, y: 5 };
  println!("Generic Point -> {:?}", p);
  println!("Coordinates Tuple --> {:?}", p.get_coordinates());
}

pub fn custom_point() {
  let p = CustomPoint { x: 5, y: 5.7 };
  println!("Generic Point -> {:?}", p);
  println!("Coordinates Tuple --> {:?}", p.get_coordinates());
}
