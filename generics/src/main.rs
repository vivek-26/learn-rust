extern crate generics;

use generics::concrete;
use generics::largest;
use generics::lifetimes;
use generics::structs;
use generics::traits;

fn main() {
  let list = vec![23, 50, 65, 67, 70, 73];
  println!(
    "Generic Largest Function --> {}",
    largest::generic_largest(&list)
  );

  structs::point();
  structs::custom_point();

  concrete::concrete_types();
  traits::traits_example();

  lifetimes::longest_string();
  lifetimes::lifetimes_struct();
}
