pub fn generic_largest<T: PartialOrd + Copy>(list: &[T]) -> T {
  let mut largest = list[0];
  for &n in list {
    if n > largest {
      largest = n;
    }
  }
  largest
}
