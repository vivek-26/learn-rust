trait Summary {
  fn summarize(&self) -> String; // This trait must be implemented.
  fn summarize_author(&self) -> String {
    format!("Read more...")
  }
}

struct NewsArticle {
  headline: String,
  content: String,
  author: String,
  location: String,
}

impl Summary for NewsArticle {
  fn summarize(&self) -> String {
    format!(
      "{}: {}, {} ({})",
      self.headline,
      self.content,
      self.summarize_author(),
      self.location
    )
  }

  fn summarize_author(&self) -> String {
    format!("by {}", self.author)
  }
}

struct Tweet {
  username: String,
  content: String,
  reply: bool,
  retweet: bool,
}

impl Summary for Tweet {
  fn summarize(&self) -> String {
    format!("{}: {}", self.username, self.content)
  }
}

trait Property {
  fn property(&self) -> String {
    String::from("This is default implementation of trait Property")
  }
}

struct DefImpl {
  prop: String,
}

impl Property for DefImpl {}

// Traits are like interfaces in other languages.
pub fn traits_example() {
  let news = NewsArticle {
    headline: String::from("India Wins"),
    content: String::from("India has won the world cup!"),
    author: String::from("Vivek Kumar"),
    location: String::from("USA"),
  };

  println!("Traits --> {}", news.summarize());

  let tweet = Tweet {
    username: String::from("Vivek Kumar"),
    content: String::from("Checkout my new Rust library!"),
    reply: true,
    retweet: false,
  };

  println!("Traits --> {}", tweet.summarize());

  let def_impl = DefImpl {
    prop: String::from("Prop"),
  };

  println!("Traits --> Default Implementation: {}", def_impl.property());
}
