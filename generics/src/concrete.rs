#[derive(Debug)]

struct Point<T> {
  x: T,
}

impl Point<f32> {
  fn number(&self) -> &f32 {
    &self.x
  }
}

impl Point<u32> {
  fn number(&self) -> &u32 {
    &self.x
  }
}

// fn number can declared multiple times since they have different
// concrete types in generics.
pub fn concrete_types() {
  let float_point = Point { x: 10.7 };
  let integer_point = Point { x: 10 };

  println!("Float Point --> {:?}", float_point.number());
  println!("Integer Point --> {:?}", integer_point.number());
}
