struct Data<'a> {
  part: &'a str,
}

impl<'a> Data<'a> {
  fn get_part(&self, other: &str) -> &str {
    println!("Other Data --> {}", other);
    self.part
  }
}

fn longest<'a>(x: &'a str, y: &'a str) -> &'a str {
  if x.len() > y.len() {
    x
  } else {
    y
  }
}

pub fn longest_string() {
  let str_1 = "Rust Programming Language";
  let str_2 = "Golang";
  let result = longest(str_1, str_2);
  println!("Longest String Using Lifetimes -> {}", result);
}

pub fn lifetimes_struct() {
  let str_1 = "Some part of a sentence";
  let data = Data { part: str_1 };
  println!(
    "Lifetimes in Struct --> {}",
    data.get_part("Rust is awesome")
  );
}
