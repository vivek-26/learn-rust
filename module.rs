// Bringing name into scope with use keyword.
pub mod a {
  pub mod series {
    pub mod of {
      pub fn nested_module() {}
    }
  }
}

use a::series::of::nested_module;

// use TrafficLight::{Red, Yellow};
use TrafficLight::*;

#[derive(Debug)]
enum TrafficLight {
  Red,
  Yellow,
  Green,
}

fn main() {
  nested_module();

  println!("{:?} {:?} {:?}", Red, Yellow, Green);
}
