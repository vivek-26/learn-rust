// Rust doesn't have `null`.
// Options enum is present by default.

fn main() {
  let a = Some(1);
  let b = Some("Some Value");
  let mut c: Option<u8> = None;
  c = Some(2);

  println!("{:?} {:?} {:?}", a, b, c);
}
