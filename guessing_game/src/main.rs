extern crate rand;
use rand::Rng;
use std::io;

fn main() {
  println!("GUESS A NUMBER");
  let secret = rand::thread_rng().gen_range(1, 10);

  loop {
    println!("Your guess --> ");
    let mut guess = String::new();
    io::stdin()
      .read_line(&mut guess)
      .expect("Failed to read input");
    let guess: i32 = guess.trim().parse().expect("Failed to convert");
    if guess == secret {
      println!("Correct answer!");
      break;
    } else {
      println!("Try Again!");
      if guess > secret {
        println!("You guessed a higher number.");
      } else {
        println!("You guessed a lower number.");
      }
    }
  }
}
