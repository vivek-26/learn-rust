// Tuples are compound type.
// Tuples have fixed length. Once declared, they cannot grow or shrink in size.
// Tuple index starts from 0.
// When you assign a tuple to a variable, it is called `destructing`.
fn main() {
  let a = (220, true, 8.5);
  println!("{:?}", a);
  println!("{:?}", a.0);

  let b: (u16, bool, f64) = (220, true, 8.5);
  println!("{:?}", b);

  print(a);
  destructing(a);
}

// Function argument --> tuple
fn print(x: (u16, bool, f64)) {
  println!("Print Function --> {:?}", x);
}

fn destructing(x: (u16, bool, f64)) {
  let (a, b, c) = x;
  println!("{} {} {}", a, b, c);
}
