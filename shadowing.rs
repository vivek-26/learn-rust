fn main() {
  let a: u8 = 10;
  println!("Before Shadowing, a --> {}", a);
  let a: u8 = 20;
  println!("After Shadowing, a --> {}", a);

  // Shadowing is different than `mut`.
  // Shadowing allows having different types.
  // Shadowing is useful for accepting user inputs.
  let b: u8 = 10;
  println!("Before Shadowing, b --> {}", b);
  let b: char = 'A';
  println!("After Shadowing, b --> {}", b);
}
