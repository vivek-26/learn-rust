// References
// Using references, we can refer a value without taking ownership of it.
// `&` ampersands are references.

// The scope in which the reference variable is valid is the same as any
// function parameter's scope.

// We don't drop what the references points to, when it goes out of scope
// because we don't have ownership.

// We don't need to return the values in order to give the ownership back
// since we never had ownership.

fn main() {
  let mut s = String::from("Rust is awesome");
  print(&s);
  println!(
    "Value of s can be used after passing it to a function as well: {}",
    s
  );
  print_mutability(&mut s);
  println!("After calling print mutability: {}", s);
}

// Here, str_1 is borrowing the value of s from main fn.
fn print(str_1: &String) {
  println!("Inside Print Function: {}", str_1);
} // When print fn ends, only reference str_1 is dropped, the value it points
  // to is never dropped. Because `s` is the owner of data in main fn.

fn print_mutability(str_1: &mut String) {
  println!("Inside Print Mutability Function: {}", str_1);
  str_1.push_str("!");
}
